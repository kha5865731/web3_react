"use client";
import React from "react";
import Menu from "./component/menu";
import InputBox from "./component/inputBox";
import Ethers from "./component/ethers";

export default function Home() {
  return (
    <main>
      <Menu />
      <Ethers />
      <h1 className='text-center text-4xl m-8 text-gray-100'> JDI one</h1>
      <InputBox />
      

    </main>
  )
}
