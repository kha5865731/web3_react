"use client";
import React,{useState} from "react";

export default function Home(){

  const [name, setName] = useState('' );
  const [email, setEmail] = useState('');
  const [Birthday, setBirthday] = useState('');
  const [Git, setGit] = useState('');
  const [data, setData] = useState([]); // Mảng lưu trữ dữ liệu đã nhập

  const handleSubmit = () => {
    if( (name && email && Birthday && Git).trim().length == 0)
    {
      console.log("can't output");

    }else{
        // Thêm dữ liệu đã nhập vào mảng
      setData([...data, { name, email,Birthday,Git}]);
      // Reset các ô input sau khi nhấn Submit
      setName('');
      setEmail('');
      setBirthday('');
      setGit('');
    }
    
  };

  return (
    <main>
        <table className="m-8 absolute top-25 right-0 bg-blue-700 ">
          <thead>
            <tr>
              <th className="border border-gray-300 p-2 text-white">Name</th>
              <th className="border border-gray-300 p-2 text-white">Email</th>
              <th className="border border-gray-300 p-2 text-white">Birthday</th>
              <th className="border border-gray-300 p-2 text-white">Git</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item, index) => (
              <tr key={index}>
                <td className="border border-gray-300 p-2 text-white">{item.name}</td>
                <td className="border border-gray-300 p-2 text-white">{item.email}</td>
                <td className="border border-gray-300 p-2 text-white">{item.Birthday}</td>
                <td className="border border-gray-300 p-2 text-white">{item.Git}</td>
              </tr>
            ))}
          </tbody>
      </table>

      <div className='ms-8 space-x-4 space-x-reverse boderBox'>
          <div className=' mt-6 text-xl flex '>
          <div className='ml-4 mr-5 pt-2'>Name</div>
          <input
            type="text"
            placeholder="Enter your Name"
            className="border border-gray-300 p-2 rounded-lg"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className='ml-10 mt-6 text-xl flex'>
          <div className='ml-4 mr-5 pt-2'>Email</div>
          <input
            type="email"
            placeholder="Enter your Email"
            className="border border-gray-300 p-2 rounded-lg"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className='ml-10 mt-6 text-xl flex'>
          <div className='ml-4 mr-5 pt-2'>Birthday</div>
          <input
            type="date"
            placeholder="Enter your Birthday"
            className="border border-gray-300 p-2 rounded-lg"
            value={Birthday}
            onChange={(e) => setBirthday(e.target.value)}
          />
        </div>
        <div className='ml-10 mt-6 text-xl flex'>
          <div className='ml-4 mr-10 pt-2'>Git</div>
          <input
            type="text"
            placeholder="Enter your Git"
            className="border border-gray-300 p-2 rounded-lg"
            value={Git}
            onChange={(e) => setGit(e.target.value)}
          />
        </div>

      </div>

      <div className='text-center m-4'>
        <button onClick={handleSubmit} className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 md:px-5 md:py-2.5 mr-1 md:mr-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">
          Submit
        </button>
      </div>

    </main>
  );
}